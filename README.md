# CGeoMathBase

## Install for Linux:
git clone https://gitlab.com/SMP33/geomath.git  
cd geomath  
mkdir build  
cd build  
cmake ..  
make  
sudo make install  

## Usage:

find_package( GeoMath REQUIRED )  
include_directories( ${CommonGeoMath_INCLUDE_DIRS} )  

add_executable( test test.cpp )  
target_link_libraries( test GeoMath::Common )  