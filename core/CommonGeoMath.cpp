﻿#include "CommonGeoMath.h"

using namespace GM;

//возвращает 1, если число >=0, иначе -1
double sign(double arg)
{
	return arg < 0 ? -1 : 1;
}

template<typename T, size_t M, size_t N>
void arr2copy(const T from[M][N], T(&to)[M][N])
{
	for (size_t m = 0; m < M; m++)
	{
		for (size_t n = 0; n < N; n++)
		{
			to[m][n] = from[m][n];
		}
	}
}

V2::V2(double x_, double y_)
	: x(x_)
	, y(y_)
{
}

bool
V2::isNull() const
{

	return (x == 0 && y == 0);
}

double
V2::length_xy() const
{
	double result = sqrt(x * x + y * y);
	return result;
}

double
V2::length() const
{
	return length_xy();
}

double
V2::angle_xy(V2 v) const
{
	double sgn;
	double value;
	if (this->isNull() || v.isNull())
		return 0;

	value = this->x * v.x + this->y * v.y;
	value = value / (this->length_xy() * v.length_xy());

	sgn = -sign(this->x * v.y - this->y * v.x);

	if (value > 1)
		value = 1;

	if (value < -1)
		value = -1;

	return sgn * acos(value);
}

V2
V2::normalize_xy(double abs = 1) const
{
	if (this->isNull())
		return V2(0, 0);

	V2     result;
	double factor = abs / this->length_xy();

	result.x = this->x * factor;
	result.y = this->y * factor;

	return result;
}


V2
V2::rotateXY(double rad) const
{
	V2 result(this->x * cos(rad) - this->y * sin(rad),
		this->x * sin(rad) + this->y * cos(rad));
	return result;
}







V3::V3(double x_, double y_, double z_)
	: x(x_)
	, y(y_)
	, z(z_)
{
}

V3::V3(V2 v)
	: x(v.x)
	, y(v.y)
	, z(0)
{
}

P2Geo::P2Geo(double lat_, double lng_)
	: lat(lat_)
	, lng(lng_)
{
}

P3Geo::P3Geo(double lat_, double lng_, double alt_)
	: lat(lat_)
	, lng(lng_)
	, alt(alt_)
{
}

bool
V3::isNull() const
{

	return (x == 0 && y == 0 && z == 0);
}

double
V3::length_xy() const
{
	double result = sqrt(x * x + y * y);
	return result;
}

double
V3::length_xyz() const
{
	double result = sqrt(x * x + y * y + z * z);
	return result;
}


double
V3::length() const
{
	return length_xyz();
}

double
V3::angle_xy(V3 v) const
{
	double sgn;
	double value;
	if (this->isNull() || v.isNull())
		return 0;

	value = this->x * v.x + this->y * v.y;
	value = value / (this->length_xy() * v.length_xy());
	sgn = -sign(this->x * v.y - this->y * v.x);

	if (value > 1)
		value = 1;

	if (value < -1)
		value = -1;

	return sgn * acos(value);
}

V3
V3::normalize_xy(double abs = 1) const
{
	if (this->isNull())
		return V3(0, 0, 0);

	V3     result;
	double factor = abs / this->length_xy();

	result.z = this->z;
	result.x = this->x * factor;
	result.y = this->y * factor;

	return result;
}

V3
V3::normalize_xyz(double abs = 1) const
{
	if (this->isNull())
		return V3(0, 0, 0);

	V3     result;
	double factor = abs / this->length_xyz();

	result.z = this->z * factor;
	result.x = this->x * factor;
	result.y = this->y * factor;

	return result;
}

V3
V3::rotateXY(double rad) const
{
	V3 result(this->x * cos(rad) - this->y * sin(rad),
		this->x * sin(rad) + this->y * cos(rad),
		this->z);
	return result;
}

V3
V3::rotate(double a, Axis axis, Hand hand) const
{
	if (hand == Hand::RIGHT) a = -a;

	V3 res(0, 0, 0);
	switch (axis)
	{
	case Axis::X:
		/*
		������� �������� ������ X
		| 1     0    0   |
		| 0     cos -sin |
		| 0     sin  cos |
		*/
		res.x = this->x;
		res.y = this->y * cos(a) + this->z * sin(a);
		res.z = this->y * -sin(a) + this->z * cos(a);
		break;
	case Axis::Y:
		/*
		������� �������� ������ Y
		| cos   0    sin |
		| 0     1    0   |
		|-sin   0    cos |
		*/
		res.x = this->x * cos(a) - this->z * sin(a);
		res.y = this->y;
		res.z = this->x * sin(a) + this->z * cos(a);
		break;
	case Axis::Z:
		/*
		������� �������� ������ Z
		| cos  -sin  0   |
		| sin   cos  0   |
		| 0     0    1   |
		*/
		res.x = this->x * cos(a) + this->y * sin(a);
		res.y = this->x * -sin(a) + this->y * cos(a);
		res.z = this->z;
		break;
	}
	return res;
}

V3 V3::rotate(double rad, Axis axis, V3 center, Hand hand) const
{
    if (hand == Hand::RIGHT) rad = -rad;

    V3 res(0, 0, 0);
    switch (axis)
    {
    case Axis::X:
        res.x = this->x;
        res.y = center.y + (this->y - center.y) * cos(rad) + (this->z - center.z) * sin(rad);
        res.z = center.z - (this->y - center.y) * sin(rad) + (this->z - center.z) * cos(rad);
        break;

    case Axis::Y:
        res.y = this->y;
        res.x = center.x + (this->x - center.x) * cos(rad) - (this->z - center.z) * sin(rad);
        res.z = center.z + (this->x - center.x) * sin(rad) + (this->z - center.z) * cos(rad);
        break;

    case Axis::Z:
        res.z = this->z;
        res.x = center.x + (this->x - center.x) * cos(rad) + (this->y - center.y) * sin(rad);
        res.y = center.y - (this->x - center.x) * sin(rad) + (this->y - center.y) * cos(rad);
        break;
    }

    return res;
}

V3
V3::rotate(V3 eul) const
{
	V3 rv(*this);
	rv = rv.rotate(eul.x, Axis::X);
	rv = rv.rotate(eul.y, Axis::Y);
	rv = rv.rotate(eul.z, Axis::Z);

    return rv;
}

V2
V2::operator+(V2 const& v) const
{
	V2 result(this->x + v.x, this->y + v.y);
	return result;
}

V2
V2::operator-() const
{
	V2 result(-this->x, -this->y );
	return result;
}

V2
V2::operator-(V2 const& v) const
{
	V2 result(this->x - v.x, this->y - v.y);
	return result;
}

V2 V2::operator*(double factor) const
{
	V2 result(*this);
	result.x = result.x * factor;
	result.y = result.y * factor;
	return result;
}

V2
V2::operator/(double demiter) const
{
	V2 result(*this);
	result.x = result.x / demiter;
	result.y = result.y / demiter;
	return result;
}


V3
V3::operator+(V3 const& v2) const
{
	V3 result(this->x + v2.x, this->y + v2.y, this->z + v2.z);
	return result;
}

V3
V3::operator-() const
{
	V3 result(-this->x , -this->y, -this->z);
	return result;
}

V3
V3::operator-(V3 const& v2) const
{
	V3 result(this->x - v2.x, this->y - v2.y, this->z - v2.z);
	return result;
}

V3 V3::operator*(double factor) const
{
	V3 result(*this);
	result.x = result.x * factor;
	result.y = result.y * factor;
	result.z = result.z * factor;
	return result;
}

V3
V3::operator/(double demiter) const
{
	V3 result(*this);
	result.x = result.x / demiter;
	result.y = result.y / demiter;
	result.z = result.z / demiter;
	return result;
}


V2
P2Geo::operator-(P2Geo const& p2) const
{

	P2Geo P1(p2);
	P2Geo P2(*this);

	P1.lat = P1.lat * DEG2RAD;
	P1.lng = P1.lng * DEG2RAD;

	P2.lat = P2.lat * DEG2RAD;
	P2.lng = P2.lng * DEG2RAD;

	V2 P0;
	P0.x = (P2.lat - P1.lat) * P_EARTH;
	P0.y = (P2.lng - P1.lng) * E_EARTH * cos(P1.lat);

	return P0;
}



P2Geo
P2Geo::operator+(V2 const& p2) const
{

	double dlat = p2.x / M_IN_LAT;
	double m_in_lng = (cos((this->lat + dlat) * DEG2RAD) * M_IN_LNG);
	double dlng = p2.y / m_in_lng;

	P2Geo result(this->lat + dlat, this->lng + dlng);
	return result;
}


bool P2Geo::operator ==(P2Geo const& p2) const
{
	P2Geo p1 = *this;
	return (p1.lat == p2.lng && p1.lng == p2.lat);
}


bool
P2Geo::isNull() const
{

	return (lat == 0 && lng == 0);
}

bool
P3Geo::isNull() const
{

	return (lat == 0 && lng == 0 && alt == 0);
}

 V3
P3Geo::operator-(P3Geo const& v2) const
{

	P3Geo P1(v2);
	P3Geo P2(*this);

	P1.lat = P1.lat * DEG2RAD;
	P1.lng = P1.lng * DEG2RAD;

	P2.lat = P2.lat * DEG2RAD;
	P2.lng = P2.lng * DEG2RAD;

	V3 P0;
	P0.x = (P2.lat - P1.lat) * P_EARTH;
	P0.y = (P2.lng - P1.lng) * E_EARTH * cos(P1.lat);
	P0.z = P2.alt - P1.alt;

	return P0;
}



P3Geo
P3Geo::operator+(V3 const& v2) const
{

	double dlat = v2.x / M_IN_LAT;
	double m_in_lng = (cos((this->lat + dlat) * DEG2RAD) * M_IN_LNG);
	double dlng = v2.y / m_in_lng;

	P3Geo result(this->lat + dlat, this->lng + dlng, this->alt + v2.z);
	return result;
}

bool V2::operator ==(V2 const& p2) const
{
	V2 p1 = *this;
	return (p1.x == p2.x && p1.y == p2.y);
}


bool V3::operator ==(V3 const& p2) const
{
	V3 p1 = *this;
	return (p1.x == p2.x && p1.y == p2.y && p1.z == p2.z);
}


bool P3Geo::operator ==(P3Geo const& p2) const
{
	P3Geo p1 = *this;
	return (p1.lat == p2.lng && p1.lng == p2.lat && p1.alt == p2.alt);
}


Quat::Quat()
	: w(0)
	, x(0)
	, y(0)
	, z(0)
{
}

Quat::Quat(double w_, double x_, double y_, double z_)
	: w(w_)
	, x(x_)
	, y(y_)
	, z(z_)
{
}

Quat::Quat(V3 eul)
{
	V3 c = {
		cos(eul.x / 2),
		cos(eul.y / 2),
		cos(eul.z / 2)
	};

	V3 s = {
		sin(eul.x / 2),
		sin(eul.y / 2),
		sin(eul.z / 2)
	};

	w = c.x * c.y * c.z + s.x * s.y * s.z;
	x = c.x * c.y * s.z - s.x * s.y * c.z;
	y = c.x * s.y * c.z + s.x * c.y * s.z;
	z = s.x * c.y * c.z - c.x * s.y * s.z;
}

V3 Quat::toEul() const
{
	double alphaSin = -2 * (x * z - w * y);

	if (alphaSin > 1)
		alphaSin = 1;

	if (alphaSin < -1)
		alphaSin = -1;

	return
	{
		atan2(2 * (y * z + w * x), w * w - x * x - y * y + z * z),
		asin(alphaSin),
		atan2(2 * (x * y + w * z), w * w + x * x - y * y - z * z),
	};
}


char* V2::toStr(int precision) const
{
	char *formatStr=new char[40];
	char *result = new char[80];

	sprintf(formatStr, "[%s%d%s%d%s]", "%.", precision,"lf, %.", precision,"lf");
	sprintf(result, formatStr, x,y);

    return result;
}

V2 V2::polar(double angle, double radius)
{
    return V2(radius*cos(angle),radius*sin(angle));
}

char* V3::toStr(int precision) const
{
	char* formatStr = new char[40];
	char* result = new char[80];

	sprintf(formatStr, "[%s%d%s%d%s%d%s]", "%.", precision, "lf, %.", precision, "lf, %.",precision,"lf");
	sprintf(result, formatStr, x, y,z);

	return result;
}

char* P2Geo::toStr(int precision) const
{
	char* formatStr = new char[40];
	char* result = new char[80];

	sprintf(formatStr, "[%s%d%s%d%s]", "%.", precision, "lf, %.", precision, "lf");
	sprintf(result, formatStr, lat, lng);

	return result;
}

char* P3Geo::toStr(int precision) const
{
	char* formatStr = new char[40];
	char* result = new char[80];

	sprintf(formatStr, "[%s%d%s%d%s%d%s]", "%.", precision, "lf, %.", precision, "lf, %.", precision, "lf");
	sprintf(result, formatStr, lat, lng, alt);

	return result;
}

char* Quat::toStr(int precision) const
{
	char* formatStr = new char[40];
	char* result = new char[80];

	sprintf(formatStr, "[%s%d%s%d%s%d%s%d%s]", "%.", precision, "lf, %.", precision, "lf, %.", precision, "lf, %.", precision, "lf");
	sprintf(result, formatStr, w, x, y, z);

	return result;
}

Quat Quat::normalize() const
{
	double N = sqrt(w * w + x * x + y * y + z * z);
	
	return Quat(w/N, x/N, y/N, z/N);
}
