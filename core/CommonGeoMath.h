#ifndef COMMONGEOMATH_H
#define COMMONGEOMATH_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>


namespace GM
{

	const double Pi = 3.1415926535897932384626433832795;    ///<  Pi

	const double E_EARTH = 6378137.0;   	///< �������������� ������ �����
	const double P_EARTH = 6356779.0;   	///< �������� ������ �����

	const double E_length = E_EARTH * 2 * Pi;   	///< ����� ��������
	const double P_length = P_EARTH * 2 * Pi;   	///< 

	const double M_IN_LAT = P_length / 360;   	///<  ����� ������ ����� ������� ������ 
	const double M_IN_LNG = E_length / 360;   	///<  ����� ������ � ����� ������� �������

	const double DEG2RAD = 0.01745329252;   	///<     
	const double RAD2DEG = 57.2957795129;   	///<     

	/// 
    enum Axis
	{
		X,  ///<     
		Y,  ///<     
		Z	///<  
	}
	;


	///      ,     
    enum Hand
	{
		LEFT,   ///<    
		RIGHT	///<    
	}
	;

	///  
	class V2
	{
	public:
		double x;
		double y;

		V2(double x_ = 0, double y_ = 0);

        bool isNull() const;
        double	length_xy() const;
        double	length() const;
        double angle_xy(V2 v = { 1,0 }) const;

        V2 normalize_xy(double abs) const;
        V2 rotateXY(double rad) const;

		V2 operator-() const;

        V2 operator+(V2 const& v) const;
        V2 operator-(V2 const& v) const;
        V2 operator*(double factor) const;
        V2 operator/(double factor) const;

		bool operator ==(V2 const& p2) const;

		char* toStr(int precision = 3) const;

        ///
        /// \brief polar Return V2 in polar system
        /// \param angle angle in rad
        /// \param radius radius
        /// \return V2(radius*cos(angle),radius*sin(angle))
///
        static V2 polar(double angle, double radius=1);
	};

	class V3
	{
	public:
		double x;
		double y;
		double z;

		V3(double x_ = 0, double y_ = 0, double z_ = 0);
		V3(V2 v);

        bool isNull() const;
        double	length_xy() const;
        double	length_xyz() const;
        double	length() const;
        double angle_xy(V3 v = { 1,0,0 }) const;

        V3 normalize_xy(double abs) const;
        V3 normalize_xyz(double abs) const;
        V3 rotateXY(double rad) const;

        V3 rotate(double rad, Axis axis, Hand hand = Hand::RIGHT) const;
        V3 rotate(double rad, Axis axis, V3 center, Hand hand = Hand::RIGHT) const;
        V3 rotate(V3 eul) const;

		V3 operator-() const;

        V3 operator+(V3 const& v2) const;
        V3 operator-(V3 const& v2) const;
        V3 operator*(double factor) const;
        V3 operator/(double factor) const;

        bool operator ==(V3 const& v) const;
		char* toStr(int precision = 3) const;
	};


	class P2Geo
	{
	public:
		double lat;
		double lng;

        bool isNull() const;

        P2Geo(double lat_ = 0, double lng_ = 0);

        V2 operator-(P2Geo const& p2) const;
        P2Geo operator+(V2 const& p2) const;
        bool operator ==(P2Geo const& p2) const;
		char* toStr(int precision = 3) const;
	};

	class P3Geo
	{
	public:
		double lat;
		double lng;
		double alt;

        bool isNull() const;

        P3Geo(double lat_ = 0, double lng_ = 0, double alt_ = 0);

        V3 operator-(P3Geo const& v2) const;
        P3Geo operator+(V3 const& v2) const;
        bool operator ==(P3Geo const& p2) const;
		char* toStr(int precision = 3) const;
	};


	class Quat
	{
	public:
		double w;
		double x;
		double y;
		double z;

		Quat();
		Quat(double w, double x, double y, double z);
		Quat(V3 eul);
        V3 toEul() const;
		char* toStr(int precision = 3) const;
		Quat normalize() const;
	};
}

#endif //COMMONGEOMATH_H
