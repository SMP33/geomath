#include <iostream>
#include "GeoMath.h"

using namespace GM;
using namespace std;

int main(int argc, char** argv)
{
    std::cout.precision(5);
    std::cout.setf(std::ios::fixed);

    V2 a1(0,0),a2(1,1),a3(2,0);
    V2 b3(0,0),b2(1,-1),b1(2,0);

    auto t=findTransform(a1,a2,a3,
                           b1,b2,b3);

    cout<<"Offset: "<<t.offset.toStr()<<endl;
    cout<<"Rot: "<<t.rotation*RAD2DEG;



//    V3 v1(1,2,3);
//    Quat q(1,2,3,4);
//    q=Quat(sqrt(0.5),sqrt(0.5),0,0);
//    q=q.normalize();
//    V3 eul=q.toEul();

//    cout<<v1.toStr()<<endl;
//    cout<<q.toStr()<<endl;
//    cout<<eul.toStr()<<endl;
//    cout<<(v1.rotate(eul).toStr())<<endl;

	return 0;
}
