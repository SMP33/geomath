#ifndef GEOMATHROUTING_H
#define GEOMATHROUTING_H

#include "../core/CommonGeoMath.h"
#include <vector>

struct Transform2D
{
    Transform2D(GM::V2 offset=GM::V2(0,0), double rotation=0):
        offset(offset),
        rotation(rotation)
    {}

    GM::V2 offset=0;
    double rotation=0; ///rad
};

template<typename T>
T center(std::vector<T> points)
{
    if(points.size()<1)
        return T();

    if(points.size()==1)
        return points[0];

    auto t=points[1]-points[0];
    t=t-t;

    for (size_t i=1;i<points.size();i++) {
        t=t+(points[i]-points[0]);
    }

    t=t/points.size();

    return points[0]+t;
}

Transform2D findTransform(GM::V2 a1,GM::V2 b1,GM::V2 c1,GM::V2 a2,GM::V2 b2,GM::V2 c2);

#endif // GEOMATHROUTING_H
