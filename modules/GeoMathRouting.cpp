#include "GeoMathRouting.h"

using namespace GM;

Transform2D findTransform(V2 a1, V2 b1, V2 c1, V2 a2, V2 b2, V2 c2)
{
    Transform2D res;

    V2 o1=(a1+b1+c1)/3;
    V2 o2=(a2+b2+c2)/3;

    V2 offset=o2-o1;

//    printf("o1: %s\n",o1.toStr());
//    printf("o2: %s\n",o2.toStr());

    res.offset=offset;

    a1=a1-o1;
    b1=b1-o1;
    c1=c1-o1;

    a2=a2-o2;
    b2=b2-o2;
    c2=c2-o2;

    double fa=a1.angle_xy(a2);
    double fb=b1.angle_xy(b2);
    double fc=c1.angle_xy(c2);

    res.rotation=((V2::polar(fa)+V2::polar(fa)+V2::polar(fa))/3).angle_xy();

    return res;
}

